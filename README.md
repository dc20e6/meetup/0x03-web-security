## Terraform

Before running `terraform apply` in the `terraform` directory you need to create terraform/variables.tf:
```
variable "do_token" {
    default = "<your_do_token>"
}
variable "ssh_keys" {
    default = ["<id_ssh_key>", "<id_ssh_key>"]
}
```

For getting `id_ssh_key` read [API Documentation](https://developers.digitalocean.com/documentation/v2/#list-all-keys)


## Ansible

Run in the `ansible` directory.

For creating default environment:
`ansible-playbook -i '<ip>,' -u root web_default.yml`

For installing app:
`ansible-playbook -i '<ip>,' -u root web_app.yml`

Copy shell in the document root dir:
`ansible-playbook -i '<ip>,' -u root web_shells.yml`

Improve php config:
`ansible-playbook -i '<ip>,' -u root php_config.yml`

Improve nginx config:
`ansible-playbook -i '<ip>,' -u root nginx_config.yml`

Improve nginx config for restriction:
`ansible-playbook -i '<ip>,' -u root nginx_config.yml`
