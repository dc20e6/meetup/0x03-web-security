provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "dc20e6_0x03" {
  name     = "dc20e6-0x03"
  region   = "ams3"
  image    = "centos-7-x64"
  size     = "s-1vcpu-1gb"
  ssh_keys = "${var.ssh_keys}"
  tags     = ["dc20e6", "0x03"]
}

output "droplet_public_ip" {
  value = "${digitalocean_droplet.dc20e6_0x03.ipv4_address}"
}
